package com.example.threadpool.common;

import java.util.Random;

/**
 * Helper class for functions common to entire project.
 */
public class Common {
    
    /**
     * Generated random integer in specified range.
     *
     * @param boundLow  Lower bound.
     * @param boundHigh Higher bound.
     * @return Ranom integer,
     */
    public static int GenerateRandomNumber( int boundLow, int boundHigh ) {
        Random random = new Random();
        
        float rand = random.nextFloat();
        
        return ( int ) ( boundLow + ( boundHigh - boundLow ) * rand );
    }
    
    /**
     * Generates random number with specified number of digits.
     *
     * @param length Number of digits.
     * @return Random number as string.
     */
    public static String GenerateRandomNumber( int length ) {
        int boundLow = ( int ) Math.pow( 10, length - 1 );
        int boundHigh = ( int ) Math.pow( 10, length ) - 1;
        
        return String.valueOf( GenerateRandomNumber( boundLow, boundHigh ) );
    }
    
}
