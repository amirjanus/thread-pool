package com.example.threadpool.common;

/**
 * Singleton class that generates unique identifier by incrementing non atomic static integer
 * variable. Class should be used from single thread only.
 */
public class UniqueIdentifier {
    
    // Object instance.
    private static final UniqueIdentifier INSTANCE = new UniqueIdentifier();
    
    // Counter used as unique identifier.
    private static int m_counter = 0;
    
    /**
     * Private constructor to prevent creation of multiple object instances.
     */
    private UniqueIdentifier() {}
    
    /**
     * Returns new generated unique identifier.
     */
    public static int GetId() {
        return ++m_counter;
    }
    
}
