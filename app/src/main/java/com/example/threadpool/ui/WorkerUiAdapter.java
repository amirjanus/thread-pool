package com.example.threadpool.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.threadpool.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for RecyclerView that displays Worker UI.
 */
public class WorkerUiAdapter extends RecyclerView.Adapter<WorkerUiAdapter.WorkerViewHolder> {
    
    private Context m_context;
    
    private ArrayList<WorkerUi> m_workerUis;
    
    public WorkerUiAdapter() {
        m_workerUis = new ArrayList<>();
    }
    
    public WorkerUiAdapter( ArrayList<WorkerUi> workerNames ) {
        m_workerUis = workerNames;
    }
    
    @NonNull
    @Override
    public WorkerViewHolder onCreateViewHolder( @NonNull ViewGroup parent, int viewType ) {
        m_context = parent.getContext();
        
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.item_worker, parent, false );
        
        return new WorkerViewHolder( view );
    }
    
    @Override
    public void onBindViewHolder( @NonNull WorkerViewHolder holder, int position ) {
        WorkerUi current = m_workerUis.get( position );
    
        if ( !current.IsInitialized() ) {
        
            holder.m_recyclerSharedQueue.setLayoutManager( new LinearLayoutManager( m_context, RecyclerView.HORIZONTAL, false ) );
            current.InitSharedQueue( holder.m_recyclerSharedQueue );
        
            holder.m_recyclerLocalQueue.setLayoutManager( new LinearLayoutManager( m_context, RecyclerView.HORIZONTAL, false ) );
            current.InitLocalQueue( holder.m_recyclerLocalQueue );
        
            holder.m_recyclerCurrentList.setLayoutManager( new LinearLayoutManager( m_context, RecyclerView.HORIZONTAL, false ) );
            current.InitCurrentList( holder.m_recyclerCurrentList );
        
            holder.m_textName.setText( current.GetName() );
        
        }
    }
    
    @Override
    public void onBindViewHolder( @NonNull WorkerViewHolder holder, int position, @NonNull List<Object> payloads ) {
        if ( payloads.size() > 0 ) {
            
            Number i = ( Number ) payloads.get( payloads.size() - 1 );
            
            if ( i.intValue() > -1 ) {
                
                WorkerUi current = m_workerUis.get( position );
                holder.m_currentList.setText( m_context.getString( R.string.label_current_list_param, current.GetCurrentListName() ) );
                
            } else {
                holder.m_currentList.setText( R.string.label_current_list );
            }
            
        } else {
            super.onBindViewHolder( holder, position, payloads );
        }
    }
    
    @Override
    public int getItemCount() {
        return m_workerUis.size();
    }
    
    public class WorkerViewHolder extends RecyclerView.ViewHolder {
        
        // Displays name of Worker thread.
        private TextView m_textName;
        
        // Displays name of PackageList the Worker is currently processing.
        private TextView m_currentList;
        
        // These RecyclerView display PackageList names in Worker shared and local queue and Package
        // names if Worker current PackageList.
        private RecyclerView m_recyclerSharedQueue;
        private RecyclerView m_recyclerLocalQueue;
        private RecyclerView m_recyclerCurrentList;
        
        public WorkerViewHolder( @NonNull View view ) {
            super( view );
            
            m_textName = view.findViewById( R.id.text_name );
            m_currentList = view.findViewById( R.id.text_currentList );
            m_recyclerSharedQueue = view.findViewById( R.id.recycler_sharedQueue );
            m_recyclerLocalQueue = view.findViewById( R.id.recycler_local_queue );
            m_recyclerCurrentList = view.findViewById( R.id.recycler_current_list );
        }
    }
    
}
