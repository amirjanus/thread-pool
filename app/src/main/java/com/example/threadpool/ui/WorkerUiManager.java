package com.example.threadpool.ui;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Class used for managing PackageManager and Workers UI. Class is tied to RecyclerView that displays
 * list of Workers.
 */
public class WorkerUiManager {
    
    // Adapter for RecyclerView that displays Worker UI.
    private WorkerUiAdapter m_adapter;
    // Dataset for Workers UI adapter.
    private ArrayList<WorkerUi> m_workerUis;
    
    /**
     * @param recyclerView Recycler View this class will manage.
     */
    public WorkerUiManager( RecyclerView recyclerView ) {
        m_workerUis = new ArrayList<>();
        
        m_adapter = new WorkerUiAdapter( m_workerUis );
        recyclerView.setAdapter( m_adapter );
    }
    
    /**
     * Creates new WorkerUi objects in a batch.
     *
     * @param ids   WorkerUi ID array. Must match Worker ID.
     * @param names WorkerUi name array. Must match Worker thread name.
     */
    public void CreateWorkerUis( int[] ids, String[] names ) {
        if ( ids.length != names.length ) {
            throw new IllegalArgumentException( "Array length mismatch." );
        }
        
        for ( int i = 0; i < ids.length; ++i ) {
            
            m_workerUis.add( new WorkerUi( ids[i], names[i] ) );
            
            m_adapter.notifyItemInserted( i );
            
        }
    }
    
    /**
     * Removes all WorkerUi objects from managers list. This should be called when thread pool
     * ( PackageManager ) has been stopped.
     */
    public void RemoveWorkerUis() {
        m_workerUis.clear();
        
        m_adapter.notifyDataSetChanged();
    }
    
    /**
     * Adds PackageListUi to WorkerUi.
     *
     * @param workerId      Worker ID.
     * @param packageListUi List to Add.
     */
    public void AddListToSharedQueue( int workerId, PackageListUi packageListUi ) {
        WorkerUi workerUi = GetWorkerById( workerId );
        
        if ( workerUi != null ) {
            workerUi.AddListToSharedQueue( packageListUi );
        }
    }
    
    /**
     * Creates copy of passed list and adds it to WorkerUi shared queue.
     *
     * @param other List to Add.
     */
    public void AddListToSharedQueue( PackageListUi other ) {
        for ( WorkerUi workerUi : m_workerUis ) {
            
            PackageListUi list = new PackageListUi( other.GetId(), other.GetName() );
            list.CopyDatasetByValue( other );
            
            workerUi.AddListToSharedQueue( list );
            
        }
    }
    
    /**
     * Adds PackageListUi to WorkerUi local queue.
     *
     * @param workerId Worker ID.
     * @param listId   PackageList ID.
     */
    public void AddListToLocalQueue( int workerId, int listId ) {
        WorkerUi workerUi = GetWorkerById( workerId );
        
        if ( workerUi != null ) {
            workerUi.AddListToLocalQueue( listId );
        }
    }
    
    /**
     * Adds PackageListUi to WorkerUi as current list.
     *
     * @param workerId Worker ID.
     * @param listId   PackageList ID.
     */
    public void AddListAsCurrentList( int workerId, int listId ) {
        WorkerUi workerUi = GetWorkerById( workerId );
        
        if ( workerUi != null ) {
            
            workerUi.AddListAsCurrentList( listId );
            
            // We want partial update of RecyclerView for WorkerUi.
            m_adapter.notifyItemChanged( GetWorkerIndex( workerId ), listId );
            
        }
    }
    
    /**
     * Signals WorkerUi that current list in Worker has been processed.
     *
     * @param workerId Worker ID.
     * @param listId   PackageList ID.
     */
    public void CurrentListDone( int workerId, int listId ) {
        WorkerUi workerUi = GetWorkerById( workerId );
        
        if ( workerUi != null ) {
            
            workerUi.CurrentListDone( listId );
            
            m_adapter.notifyItemChanged( GetWorkerIndex( workerId ), -1 );
            
        }
    }
    
    /**
     * Signals WorkerUi that Worker has started processing Package.
     *
     * @param workerId  Worker ID.
     * @param packageId Package ID.
     */
    public void PackageProcessingStarted( int workerId, int packageId ) {
        WorkerUi workerUi = GetWorkerById( workerId );
        
        if ( workerUi != null ) {
            workerUi.PackageProcessingStarted( packageId );
        }
    }
    
    /**
     * Returns WorkerUi from WorkerUi list.
     *
     * @param workerId Worker ID.
     * @return WorkerUi object or null if not found.
     */
    private WorkerUi GetWorkerById( int workerId ) {
        for ( WorkerUi workerUi : m_workerUis ) {
            
            if ( workerUi.GetId() == workerId ) {
                return workerUi;
            }
            
        }
        
        return null;
    }
    
    /**
     * Returns WorkerUi index in WorkerUi list.
     *
     * @param workerId Worker ID.
     * @return WorkerUi index or -1 if not found.
     */
    private int GetWorkerIndex( int workerId ) {
        for ( int i = 0; i < m_workerUis.size(); ++i ) {
            
            if ( m_workerUis.get( i ).GetId() == workerId ) {
                return i;
            }
            
        }
        
        return -1;
    }
    
}
