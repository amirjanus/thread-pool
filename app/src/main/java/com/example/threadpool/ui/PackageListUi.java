package com.example.threadpool.ui;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Class manages displaying for Package or PackageList names in RecyclerView.
 */
public class PackageListUi {
    
    // Adapter for RecyclerView that this object manages.
    private PackageUiAdapter m_adapter;
    
    // Adapter dataset.
    public ArrayList<PackageUi> m_packages;
    
    // PackageListUi ID. Must match the ID in corresponding PackageList.
    public int m_id;
    // PackageListUi name. Should match the name in corresponding PackageList.
    private String m_name;
    
    /**
     * Create new PackageListUi without initializing adapter.
     *
     * @param id List ID.
     */
    public PackageListUi( int id, String name ) {
        m_id = id;
        m_name = name;
        
        m_packages = new ArrayList<>();
    }
    
    /**
     * Creates new PackageListUi with existing adapter
     *
     * @param id      List ID.
     * @param adapter Adapter to use with this list.
     */
    public PackageListUi( int id, String name, PackageUiAdapter adapter ) {
        m_id = id;
        m_name = name;
        
        m_packages = new ArrayList<>();
        
        m_adapter = adapter;
        m_adapter.SetAdapterDataset( m_packages );
    }
    
    /**
     * Creates new PackageListUi object with attaching adapter to RecyclerView. Note that this will
     * create new adapter and attach it to RecyclerView.
     *
     * @param id           List ID.
     * @param recyclerView RecyclerView to which attach this list.
     */
    public PackageListUi( int id, String name, RecyclerView recyclerView ) {
        m_id = id;
        m_name = name;
        
        m_packages = new ArrayList<>();
        
        m_adapter = new PackageUiAdapter( m_packages );
        recyclerView.setAdapter( m_adapter );
    }
    
    /**
     * Copies dataset from another PackageListUi to this PackageListUi.
     *
     * @param other PackageListUi whose dataset to copy.
     */
    public void CopyDatasetByReference( PackageListUi other ) {
        // Clear dataset in this PackageListUi.
        m_packages.clear();
        
        // Reference dataset in other PackageListUi.
        m_packages = other.m_packages;
        other.m_packages = null;
        
        // Copy ID and name.
        m_id = other.m_id;
        m_name = new String( other.m_name );
        
        // Point adapter to new dataset.
        m_adapter.SetAdapterDataset( m_packages );
    }
    
    /**
     * Copies dataset from another PackageListUi to this PackageListUi.
     *
     * @param other PackageListUi whose dataset to copy.
     */
    public void CopyDatasetByValue( PackageListUi other ) {
        // Copy ID and name.
        m_id = other.m_id;
        m_name = new String( other.m_name );
        
        // Copy dataset items.
        for ( int i = 0; i < other.GetSize(); ++i ) {
            m_packages.add( other.m_packages.get( i ) );
        }
    }
    
    /**
     * Attaches existing adapter to this list and links own dataset to that adapter dataset.
     *
     * @param adapter Adapter to attach.
     */
    public void AttachAdapter( PackageUiAdapter adapter ) {
        m_adapter = adapter;
        m_adapter.SetAdapterDataset( m_packages );
    }
    
    /**
     * Detaches adapter from this list and returns it. After that adapter will be null.
     *
     * @return This list adapter.
     */
    public PackageUiAdapter DetachAdapter() {
        PackageUiAdapter adapter = m_adapter;
        
        m_adapter = null;
        
        return adapter;
    }
    
    /**
     * Calls adapter to do partial update of RecyclerView item view.
     *
     * @param id ID of PackageUi to highlight.
     */
    public void HighlightItem( int id ) {
        int index = GetItemIndex( id );
        
        m_adapter.notifyItemChanged( index, index );
    }
    
    /**
     * Adds new item to this list.
     *
     * @param p Item to Add.
     */
    public void AddItem( PackageUi p ) {
        m_packages.add( p );
        
        if ( m_adapter != null ) {
            m_adapter.notifyItemInserted( m_packages.size() );
        }
    }
    
    /**
     * Adds new item to this list.
     *
     * @param id   PackageUi ID.
     * @param name PackageUi name.
     */
    public void AddItem( int id, String name ) {
        m_packages.add( new PackageUi( id, name ) );
        
        if ( m_adapter != null ) {
            m_adapter.notifyItemInserted( m_packages.size() );
        }
    }
    
    /**
     * Removes last item from this list.
     */
    public void RemoveItem() {
        if ( m_packages.size() > 0 ) {
            m_packages.remove( m_packages.size() - 1 );
            
            if ( m_adapter != null ) {
                m_adapter.notifyItemRemoved( m_packages.size() );
            }
        }
    }
    
    /**
     * Removes item from this list.
     *
     * @param id ID of PackageUi object to remove.
     */
    public void RemoveItem( int id ) {
        if ( m_packages.size() > 0 ) {
            
            int i;
            
            for ( i = 0; i < m_packages.size(); ++i ) {
                
                if ( m_packages.get( i ).m_id == id ) {
                    m_packages.remove( i );
                    
                    break;
                }
                
            }
            
            if ( m_adapter != null ) {
                m_adapter.notifyItemRemoved( i );
            }
        }
    }
    
    /**
     * Clears this PackageListUi dataset.
     */
    public void ClearDataset() {
        m_packages.clear();
        
        m_adapter.notifyDataSetChanged();
    }
    
    /**
     * Returns size of this PackageListUi dataset.
     *
     * @return Dataset size.
     */
    public int GetSize() {
        return m_packages.size();
    }
    
    /**
     * Returns request item at specified position in PackageListUi dataset.
     *
     * @param index Item position.
     * @return Requested item.
     */
    public PackageUi GetItem( int index ) {
        return m_packages.get( index );
    }
    
    /**
     * Returns index of request item in PackageListUi dataset.
     *
     * @param p PackageUi whose position to find.
     * @return Requested item index or -1 if not found.
     */
    public int GetItemIndex( PackageUi p ) {
        for ( int i = 0; i < m_packages.size(); ++i ) {
            
            if ( m_packages.get( i ).m_id == p.m_id ) {
                return i;
            }
            
        }
        
        return -1;
    }
    
    /**
     * Returns index of request item in PackageListUi dataset.
     *
     * @param id ID of PackageUi whose position to find.
     * @return Requested item index or -1 if not found.
     */
    public int GetItemIndex( int id ) {
        for ( int i = 0; i < m_packages.size(); ++i ) {
            
            if ( m_packages.get( i ).m_id == id ) {
                return i;
            }
            
        }
        
        return -1;
    }
    
    /**
     * Returns ID of this PackageListUi.
     */
    public int GetId() {
        return m_id;
    }
    
    /**
     * Returns name of this PackageListUi.
     */
    public String GetName() {
        return m_name;
    }
    
}
