package com.example.threadpool.ui;

import androidx.recyclerview.widget.RecyclerView;

import com.example.threadpool.common.Common;
import com.example.threadpool.common.UniqueIdentifier;

/**
 * Class used for managing PackageList UI. Class is tied to RecyclerView that displays
 * PackageListUi to which user adds or removes items.
 */
public class PackageListUiManager {
    
    private static final int MAX_ITEMS = 15;
    
    private PackageListUi m_list;
    
    /**
     * Creates new PackageListUiManager object.
     *
     * @param packagesCount Number of items to create in PackageListUi.
     * @param recyclerView  RecyclerView to which to attach PackageListUi.
     */
    public PackageListUiManager( int packagesCount, RecyclerView recyclerView ) {
        int id = UniqueIdentifier.GetId();
        String name = Common.GenerateRandomNumber( 2 );
        
        // Create new list from random ID and RecyclerView.
        m_list = new PackageListUi( id, name, recyclerView );
        
        // Add packagesCount number of items to this list.
        AddBatchToCurrentList( packagesCount );
    }
    
    /**
     * Removes current PackageListUi from manager and replaces it with new list.
     *
     * @return Current PackageListUi.
     */
    public PackageListUi DetachCurrentList() {
        // Get reference to existing list.
        PackageListUi currentList = m_list;
        
        // Create new list with random and adapter from existing list.
        int id = UniqueIdentifier.GetId();
        String name = Common.GenerateRandomNumber( 2 );
        PackageUiAdapter adapter = m_list.DetachAdapter();
        
        m_list = new PackageListUi( id, name, adapter );
        
        AddBatchToCurrentList( currentList.GetSize() );
        
        // Return reference to what was existing list.
        return currentList;
    }
    
    /**
     * Adds single item to current PackageListUi. Item value will be random generated. Item will not be
     * added if list is above MAX_ITEMS size.
     *
     * @return True if item was added, false otherwise.
     */
    public boolean AddItemToCurrentList() {
        if ( m_list.GetSize() == MAX_ITEMS ) {
            return false;
        }
        
        int id = UniqueIdentifier.GetId();
        String name = Common.GenerateRandomNumber( 2 );
        
        m_list.AddItem( new PackageUi( id, name ) );
        
        return true;
    }
    
    /**
     * Adds batch of items to current PackageListUi.
     *
     * @param batchSize Number of items to Add.
     */
    public boolean AddBatchToCurrentList( int batchSize ) {
        if ( m_list.GetSize() + batchSize > MAX_ITEMS ) {
            return false;
        }
        
        for ( int i = 0; i < batchSize; ++i ) {
            
            int id = UniqueIdentifier.GetId();
            String name = Common.GenerateRandomNumber( 2 );
            
            m_list.AddItem( new PackageUi( id, name ) );
            
        }
        
        return true;
    }
    
    /**
     * Returns item from this PackageListUi list.
     */
    public PackageUi GetListItem( int index ) {
        return m_list.GetItem( index );
    }
    
    /**
     * Removes last item from this PackageListUi list.
     */
    public void RemoveItemFromList() {
        m_list.RemoveItem();
    }
    
    /**
     * Returns this PackageListUi size.
     */
    public int GetListSize() {
        return m_list.GetSize();
    }
    
    /**
     * Returns this PackageListUi ID.
     */
    public int GetListId() {
        return m_list.GetId();
    }
    
    /**
     * Returns this PackageListUi name.
     */
    public String GetListName() {
        return m_list.GetName();
    }
    
}
