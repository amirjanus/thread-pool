package com.example.threadpool.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.threadpool.R;

import java.util.ArrayList;
import java.util.List;

public class PackageUiAdapter extends RecyclerView.Adapter<PackageUiAdapter.PackageUiViewHolder> {
    
    private ArrayList<PackageUi> m_data;
    
    public PackageUiAdapter( ) {
        m_data = new ArrayList<>();
    }
    
    public PackageUiAdapter( ArrayList<PackageUi> data ) {
        m_data = data;
    }
    
    @NonNull
    @Override
    public PackageUiViewHolder onCreateViewHolder( @NonNull ViewGroup parent, int viewType ) {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.item_package_list, parent, false );
        
        return new PackageUiViewHolder( view );
    }
    
    @Override
    public void onBindViewHolder( @NonNull PackageUiViewHolder holder, int position ) {
        PackageUi current = m_data.get( position );
        
        holder.m_name.setText( String.valueOf( current.m_name ) );
        holder.m_name.setBackgroundResource( android.R.color.holo_orange_light );
    }
    
    @Override
    public void onBindViewHolder( @NonNull PackageUiViewHolder holder, int position, @NonNull List<Object> payloads ) {
        if ( payloads.size() > 0 ) {
            holder.m_name.setBackgroundResource( android.R.color.holo_red_light );
        } else {
            super.onBindViewHolder( holder, position, payloads );
        }
    }
    
    @Override
    public int getItemCount() {
        return m_data.size();
    }
    
    public void SetAdapterDataset( ArrayList<PackageUi> list ) {
        m_data = list;
        
        notifyDataSetChanged();
    }
    
    public void RemoveAdapterDataset() {
        m_data = new ArrayList<>();
        
        notifyDataSetChanged();
    }
    
    public class PackageUiViewHolder extends RecyclerView.ViewHolder {
        
        private TextView m_name;
        
        public PackageUiViewHolder( @NonNull View itemView ) {
            super( itemView );
            
            m_name = itemView.findViewById( R.id.text_name );
        }
    }
    
}
