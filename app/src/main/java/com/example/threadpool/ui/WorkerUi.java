package com.example.threadpool.ui;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Class manages display of Worker UI.
 */
public class WorkerUi {
    
    // UI for Worker shared queue.
    private PackageListUi m_sharedQueueList;
    private ArrayList<PackageListUi> m_sharedQueue;
    
    // UI for Worker local queue.
    private PackageListUi m_localQueueList;
    private ArrayList<PackageListUi> m_localQueue;
    
    // UI for Worker current PackageList.
    private PackageListUi m_currentList;
    
    // Worker ID.
    private int m_id;
    // Worker thread name.
    private String m_name;
    
    /**
     * Creates new WorkerId object. ID and name should match ID and name of Worker this WorkerUi is
     * tied to.
     *
     * @param id   Worker ID.
     * @param name Worker thread name.
     */
    public WorkerUi( int id, String name ) {
        m_id = id;
        m_name = name;
    }
    
    /**
     * Initializes PackageListUi that displays names of PackagesLists in Worker shared queue.
     *
     * @param recyclerView RecyclerView for displaying Worker shared queue.
     */
    public void InitSharedQueue( RecyclerView recyclerView ) {
        m_sharedQueue = new ArrayList<>();
        
        m_sharedQueueList = new PackageListUi( -1, "", recyclerView );
    }
    
    /**
     * Adds PackageListUi to WorkerUi shared queue. This should be called when new PackageList is
     * added to Worker shared queue.
     *
     * @param packageListUi PackageListUi to Add to WorkerUi shared queue.
     */
    public void AddListToSharedQueue( PackageListUi packageListUi ) {
        m_sharedQueue.add( packageListUi );
        
        m_sharedQueueList.AddItem( packageListUi.GetId(), packageListUi.GetName() );
    }
    
    /**
     * Initializes PackageListUi that displays names of PackagesLists in Worker local queue.
     *
     * @param recyclerView RecyclerView for displaying Worker local queue.
     */
    public void InitLocalQueue( RecyclerView recyclerView ) {
        m_localQueue = new ArrayList<>();
        
        m_localQueueList = new PackageListUi( -1, "", recyclerView );
    }
    
    /**
     * Adds PackageListUi to WorkerUi local queue. This should be called when new PackageList is
     * added to Worker local queue.
     *
     * @param listId ID of PackageListUi to Add to WorkerUi local queue.
     */
    public void AddListToLocalQueue( int listId ) {
        // Remove list from shared queue.
        PackageListUi list = RemoveById( m_sharedQueue, listId );
        
        m_sharedQueueList.RemoveItem( listId );
        
        // Add list to local queue.
        m_localQueue.add( list );
        
        m_localQueueList.AddItem( list.GetId(), list.GetName() );
    }
    
    /**
     * Initializes PackageListUi that displays names of Package in Worker current PackageList.
     *
     * @param recyclerView RecyclerView for displaying Worker current PackageList.
     */
    public void InitCurrentList( RecyclerView recyclerView ) {
        m_currentList = new PackageListUi( -1, "", recyclerView );
    }
    
    /**
     * Adds PackageListUi as WorkerUi current list. This should be called when new PackageList is
     * added to Worker as current list.
     *
     * @param listId ID of PackageListUi to Add as WorkerUi current list.
     */
    public void AddListAsCurrentList( int listId ) {
        // Remove list from local queue.
        PackageListUi list = RemoveById( m_localQueue, listId );
        
        m_localQueueList.RemoveItem( listId );
        
        // Add list as current list.
        m_currentList.CopyDatasetByReference( list );
    }
    
    /**
     * This will change the background of item in RecyclerView. That item should be the Package
     * the Worker has started processing.
     *
     * @param packageId PackageUi ID.
     */
    public void PackageProcessingStarted( int packageId ) {
        m_currentList.HighlightItem( packageId );
    }
    
    /**
     * Clears dataset of current PackageListUi.
     *
     * @param listId PackageList ID.
     */
    public void CurrentListDone( int listId ) {
        m_currentList.ClearDataset();
    }
    
    /**
     * Remove item from list and returns that item.
     *
     * @param list List from which to remove item.
     * @param id   ID of item to remove.
     * @return Removed item or null if item not found in list.
     */
    private PackageListUi RemoveById( ArrayList<PackageListUi> list, int id ) {
        for ( int i = 0; i < list.size(); ++i ) {
            
            if ( list.get( i ).GetId() == id ) {
                return list.remove( i );
            }
            
        }
        
        return null;
    }
    
    /**
     * Checks if RecyclerViews have been initialized.
     *
     * @return True if RecyclerView have been already initialized, false otherwise.
     */
    public boolean IsInitialized() {
        return m_sharedQueueList != null;
    }
    
    /**
     * Returns ID of this WorkerUI. Must match the ID of Worker.
     */
    public int GetId() {
        return m_id;
    }
    
    /**
     * Returns name of this WorkerUI. Must match the name of Worker.
     */
    public String GetName() {
        return m_name;
    }
    
    /**
     * Returns name of PackageList the Worker is currently processing.
     */
    public String GetCurrentListName() {
        if ( m_currentList != null ) {
            return m_currentList.GetName();
        } else {
            return "-1";
        }
    }
    
}