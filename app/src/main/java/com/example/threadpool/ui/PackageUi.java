package com.example.threadpool.ui;

/**
 * Class used as item in adapter dataset for displaying Package and PackageList names in RecyclerView.
 */
public class PackageUi {
    
    // Package unique ID.
    public int m_id;
    // Package name.
    public String m_name;
    
    public PackageUi( int id, String name ) {
        m_id = id;
        m_name = name;
    }
    
}
