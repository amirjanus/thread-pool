package com.example.threadpool;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.threadpool.pool.Package;
import com.example.threadpool.pool.PackageManager;
import com.example.threadpool.pool.Snorlax;
import com.example.threadpool.ui.PackageListUiManager;
import com.example.threadpool.ui.PackageUi;
import com.example.threadpool.ui.WorkerUiManager;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements PackageManager.OnListStatusChangedListener {
    
    // Tag for printing to Logcat.
    public static final String TAG = "ABC123";
    
    // Shows current PackageListUi name.
    private TextView m_textPackageListName;
    
    // Buttons for staring and stopping PackageManager.
    private Button m_btnStart;
    private Button m_btnStop;
    
    // Buttons for adding and removing items from PackageListUi and submitting it.
    private ImageButton m_btnAdd;
    private ImageButton m_btnRemove;
    private ImageButton m_btnSend;
    
    // Classes for handling user interface.
    private WorkerUiManager m_workerUiManager;
    private PackageListUiManager m_packageListUiManager;
    
    // Thread pool.
    private PackageManager m_packageManager;
    
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        
        InitViews();
        
        CreatePackageManager();
        
        InitRecyclerView();
        SetButtonListeners();
    }
    
    /**
     * Creates PackageManager object.
     */
    private void CreatePackageManager() {
        // Get number of available processors.
        int cores = Runtime.getRuntime().availableProcessors();
        
        // Cap the number of processors at 2.
        if ( cores > 2 ) {
            cores = 2;
        }
        
        m_packageManager = new PackageManager( cores );
        m_packageManager.SetOnListStatusChangedListener( this );
    }
    
    /**
     * Initializes views.
     */
    private void InitViews() {
        m_textPackageListName = findViewById( R.id.text_package_list_id );
        
        m_btnStart = findViewById( R.id.button_start );
        m_btnStop = findViewById( R.id.button_stop );
        
        m_btnAdd = findViewById( R.id.button_add );
        m_btnRemove = findViewById( R.id.button_remove );
        m_btnSend = findViewById( R.id.button_send );
    }
    
    /**
     * Sets button listeners.
     */
    private void SetButtonListeners() {
        m_btnStart.setOnClickListener( view -> StartPackageManager() );
        
        m_btnStop.setOnClickListener( view -> StopPackageManager() );
        
        m_btnAdd.setOnClickListener( view -> {
            boolean success = m_packageListUiManager.AddItemToCurrentList();
            
            if ( !success ) {
                Snackbar.make( findViewById( R.id.root ), "Max number of items reached.", Snackbar.LENGTH_LONG ).show();
            }
        } );
        
        m_btnRemove.setOnClickListener( view -> m_packageListUiManager.RemoveItemFromList() );
        
        m_btnSend.setOnClickListener( view -> SubmitList() );
    }
    
    /**
     * Initializes recycler view and its adapter.
     */
    private void InitRecyclerView() {
        // RecyclerView for Workers UI.
        RecyclerView recyclerWorkers = findViewById( R.id.recycler_workers );
        recyclerWorkers.setLayoutManager( new LinearLayoutManager( this ) );
        
        m_workerUiManager = new WorkerUiManager( recyclerWorkers );
        
        // RecyclerView for PackageList UI.
        RecyclerView recyclerPackageList = findViewById( R.id.recycler_package_list );
        recyclerPackageList.setLayoutManager( new LinearLayoutManager( this, RecyclerView.HORIZONTAL, false ) );
        
        m_packageListUiManager = new PackageListUiManager( 15, recyclerPackageList );
        
        // Display PackageListUi name.
        m_textPackageListName.setText( getString( R.string.label_package_list, m_packageListUiManager.GetListName() ) );
    }
    
    /**
     * Starts PackageManager and displays available worker threads.
     */
    private void StartPackageManager() {
        if ( !m_packageManager.IsStarted() ) {
            
            m_packageManager.Start();
            m_workerUiManager.CreateWorkerUis( m_packageManager.GetWorkerIds(), m_packageManager.GetWorkerNames() );
            
        }
    }
    
    /**
     * Stops PackageManager and clears display of available worker threads.
     */
    private void StopPackageManager() {
        if ( m_packageManager.IsStarted() ) {
            m_packageManager.Stop();
            
            m_workerUiManager.RemoveWorkerUis();
        }
    }
    
    /**
     * Creates Packages using ID and name from current PackageListUi.
     *
     * @return List of Packages.
     */
    private ArrayList<Package> GeneratePackages() {
        ArrayList<Package> packages = new ArrayList<>();
        
        for ( int i = 0; i < m_packageListUiManager.GetListSize(); ++i ) {
            
            PackageUi pUi = m_packageListUiManager.GetListItem( i );
            
            Package p = new Snorlax( pUi.m_id, pUi.m_name );
            
            packages.add( p );
        }
        
        return packages;
    }
    
    /**
     * Creates new PackageList and fills it with supplied Packages.
     *
     * @param packages List of Packages.
     * @return PackagesList.
     */
    private PackageManager.PackageList GeneratePackageList( ArrayList<Package> packages ) {
        // Get new PackageList object via PackageManager.
        PackageManager.PackageList list = m_packageManager.GetPackageList();
        
        // PackageList needs to have same ID and name as PackageListUi so we can track it.
        list.SetId( m_packageListUiManager.GetListId() );
        list.SetName( m_packageListUiManager.GetListName() );
        
        for ( int i = 0; i < packages.size(); ++i ) {
            list.Add( packages.get( i ) );
        }
        
        return list;
    }
    
    /**
     * Creates new PackageList for PackageManager thread pool and creates new PackageListUi from
     * current PackageListUi for WorkerUiManager.
     */
    private void SubmitList() {
        // Check is PackageManager started.
        if ( !m_packageManager.IsStarted() ) {
            Snackbar.make( findViewById( R.id.root ), "Start Package Manager first.", Snackbar.LENGTH_LONG ).show();
            
            return;
        }
        
        // Check if any packages are added to current PackageListUi.
        if ( m_packageListUiManager.GetListSize() == 0 ) {
            Snackbar.make( findViewById( R.id.root ), "Add some Packages to Package List", Snackbar.LENGTH_LONG ).show();
            
            return;
        }
        
        // Create new PackageList from current PackageUiList.
        ArrayList<Package> packages = GeneratePackages();
        
        PackageManager.PackageList packageList = GeneratePackageList( packages );
        
        // Send current PackageListUi to WorkerUiManager.
        m_workerUiManager.AddListToSharedQueue( m_packageListUiManager.DetachCurrentList() );
        
        // Send PackageList to PackageManager.
        m_packageManager.SubmitPackageList( packageList );
        
        // Update TextView with new PackageListUi name.
        m_textPackageListName.setText( getString( R.string.label_package_list, m_packageListUiManager.GetListName() ) );
    }
    
    /**
     * Notifies when PackageList has been added to Worker shared queue.
     */
    @Override
    public void ListAddedToSharedQueue( PackageManager.ListStatusData statusData ) {
        Log.d( TAG, "List ( name: " + statusData.m_listName + ", id: " + statusData.m_listId + " ) " +
                "added to " + statusData.m_workerName + " shared queue " );
    }
    
    /**
     * Notifies when PackageList has been added to Worker shared queue.
     */
    @Override
    public void ListAddedToLocalQueue( PackageManager.ListStatusData statusData ) {
        Log.d( TAG, "List ( name: " + statusData.m_listName + ", id: " + statusData.m_listId + " ) " +
                "added to " + statusData.m_workerName + " local queue " );
        
        m_workerUiManager.AddListToLocalQueue( statusData.m_workerId, statusData.m_listId );
    }
    
    /**
     * Notifies when PackageList has been added to Worker as a current list for processing.
     */
    @Override
    public void ListAddedAsCurrentList( PackageManager.ListStatusData statusData ) {
        Log.d( TAG, "List ( name: " + statusData.m_listName + ", id: " + statusData.m_listId + " ) " +
                "added to " + statusData.m_workerName + " as current list " );
        
        m_workerUiManager.AddListAsCurrentList( statusData.m_workerId, statusData.m_listId );
    }
    
    /**
     * Notifies Worker has processed all packages in PackageList.
     */
    @Override
    public void CurrentListDone( PackageManager.ListStatusData statusData ) {
        Log.d( TAG, "List ( name: " + statusData.m_listName + ", id: " + statusData.m_listId + " ) " +
                "done on " + statusData.m_workerName );
        
        m_workerUiManager.CurrentListDone( statusData.m_workerId, statusData.m_listId );
    }
    
    /**
     * Notifies when Worker has started processing Package from its current list.
     */
    @Override
    public void PackageProcessingStarted( PackageManager.ListStatusData statusData ) {
        Log.d( TAG, "Package ( name: " + statusData.m_packageName + ", id: " + statusData.m_packageId + " ) " +
                "processing started on " + statusData.m_workerName );
        
        m_workerUiManager.PackageProcessingStarted( statusData.m_workerId, statusData.m_packageId );
    }
    
}
