package com.example.threadpool.pool;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * This is the base class for creating classes that will be executed on Worker threads.
 */
public abstract class Package {
    
    // Implement this interface to get notified when Package has been processed,
    public interface PackageProcessedListener {
        void OnProcessed( Object result );
    }
    
    // Reference to class that implement PackageProcessedListener interface.
    private PackageProcessedListener m_listener;
    
    // Handler to communicate with android UI thread.
    private Handler m_handler;
    
    // Result of Process() function.
    private Object m_result;
    
    // Name of this package.
    private String m_name;
    // ID of this package.
    private int m_id;
    
    /**
     * Create new Package object with given ID and name.
     *
     * @param id   Package ID.
     * @param name Package name.
     */
    public Package( int id, String name ) {
        m_id = id;
        m_name = name;
    }
    
    /**
     * Internal function used by PackageManager. Called by Worker to process this Package.
     */
    public void _Process() {
        m_result = Process();
        
        if ( m_listener != null ) {
            
            // Obtain message for Handler. Second argument is result of user defined Package:Process() function.
            Message message = m_handler.obtainMessage( 0, m_result );
            message.sendToTarget();
            
        }
    }
    
    /**
     * Code defined in this function will be executed on Worker thread.
     *
     * @return Result of this function. Return null if function doesn't return any value.
     */
    public abstract Object Process();
    
    /**
     * Sets reference to class that implements PackageProcessedListener interface and creates Handler.
     *
     * @param listener Class that implements PackageProcessedListener interface.
     */
    public void SetPackageProcessedListener( PackageProcessedListener listener ) {
        m_listener = listener;
        
        if ( m_handler == null ) {
            
            m_handler = new Handler( Looper.getMainLooper() ) {
                @Override
                public void handleMessage( Message msg ) {
                    m_listener.OnProcessed( msg.obj );
                }
            };
            
        }
    }
    
    /**
     * Removes reference to class that implements PackageProcessedListener interface.
     */
    public void RemovePackageProcessedListener() {
        m_listener = null;
        m_handler = null;
    }
    
    /**
     * Returns result of Process() function if you don't want to get it through PackageProcessedListener
     * interface.
     */
    public Object GetResult() {
        return m_result;
    }
    
    /**
     * Return ID of this Package.
     *
     * @return Package ID.
     */
    public int GetId() {
        return m_id;
    }
    
    /**
     * Returns name of this Package.
     *
     * @return Package name.
     */
    public String GetName() {
        return m_name;
    }
    
}
