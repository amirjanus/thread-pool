package com.example.threadpool.pool;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.util.ArrayDeque;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Thread pool that manages worker threads.
 */
public class PackageManager {
    
    // Constants for sorting Handler messages.
    private final int STATUS_LIST_ADDED_SHARED_QUEUE = 1;
    private final int STATUS_LIST_ADDED_LOCAL_QUEUE = 2;
    private final int STATUS_LIST_ADDED_CURRENT_LIST = 3;
    private final int STATUS_CURRENT_LIST_DONE = 4;
    private final int STATUS_PACKAGE_PROCESSING_START = 5;
    
    /**
     * Interface for receiving PackageList status.
     */
    public interface OnListStatusChangedListener {
        void ListAddedToSharedQueue( ListStatusData statusData );
        void ListAddedToLocalQueue( ListStatusData statusData );
        void ListAddedAsCurrentList( ListStatusData statusData );
        void CurrentListDone( ListStatusData statusData );
        void PackageProcessingStarted( ListStatusData statusData );
    }
    
    private OnListStatusChangedListener m_statusListener;
    
    private static final int MIN_WORKERS = 1;
    private static final int MAX_WORKERS = 8;
    
    // Handler to communicate with android UI thread.
    private Handler m_handler;
    
    // Array holds worker thread objects.
    private Worker[] m_workers;
    
    // Number of worker threads.
    private int m_workersCount;
    
    // Shows are Workers created or not.
    private boolean m_isStarted;
    
    /**
     * Create PackageManager object with given number of worker threads.
     *
     * @param workersCount Number of worker threads.
     */
    public PackageManager( int workersCount ) {
        if ( workersCount < MIN_WORKERS || workersCount > MAX_WORKERS ) {
            throw new IllegalArgumentException( "Number of workers cannot be less that 1 or more than 8." );
        }
        
        m_workersCount = workersCount;
        m_workers = new Worker[workersCount];
    }
    
    /**
     * Creates worker threads.
     */
    public void Start() {
        if ( m_isStarted ) {
            return;
        }
        
        m_isStarted = true;
        
        for ( int i = 0; i < m_workersCount; ++i ) {
            
            m_workers[i] = new Worker( i, "WORKER-" + ( i + 1 ) );
            m_workers[i].Start();
            
        }
    }
    
    /**
     * Stops worker threads.
     */
    public void Stop() {
        if ( !m_isStarted ) {
            return;
        }
        
        for ( int i = 0; i < m_workersCount; ++i ) {
            
            m_workers[i].Stop();
            m_workers[i] = null;
            
        }
        
        m_isStarted = false;
    }
    
    /**
     * Returns array with names of each Worker thread.
     *
     * @return Array of Worker names.
     */
    public String[] GetWorkerNames() {
        String[] workers = new String[m_workersCount];
        
        for ( int i = 0; i < workers.length; ++i ) {
            workers[i] = m_workers[i].GetName();
        }
        
        return workers;
    }
    
    /**
     * Returns array with IDs of each Worker thread.
     *
     * @return Array of Worker IDs.
     */
    public int[] GetWorkerIds() {
        int[] workerIds = new int[m_workersCount];
        
        for ( int i = 0; i < m_workersCount; ++i ) {
            workerIds[i] = m_workers[i].GetId();
        }
        
        return workerIds;
    }
    
    /**
     * Adds PackageList to Worker shared queue.
     *
     * @param list Package list to Add.
     */
    public void SubmitPackageList( PackageList list ) {
        for ( Worker worker : m_workers ) {
            worker.SubmitPackageList( list );
        }
    }
    
    /**
     * Returns new PackageList with default capacity ( 16 ).
     *
     * @return New PackageList object.
     */
    public PackageList GetPackageList() {
        return new PackageList( m_workersCount );
    }
    
    /**
     * Returns new PackageList with given capacity.
     *
     * @param capacity Capacity of created package list.
     * @return New PackageList object.
     */
    public PackageList GetPackageList( int capacity ) {
        return new PackageList( capacity, m_workersCount );
    }
    
    /**
     * Sets reference to class that implements PackageProcessedListener interface and creates Handler.
     *
     * @param listener Class that implements OnListStatusChangedListener interface.
     */
    public void SetOnListStatusChangedListener( OnListStatusChangedListener listener ) {
        m_statusListener = listener;
        
        if ( m_handler == null ) {
            
            m_handler = new Handler( Looper.getMainLooper() ) {
                @Override
                public void handleMessage( Message msg ) {
                    switch ( msg.what ) {
                        case STATUS_LIST_ADDED_SHARED_QUEUE:
                            m_statusListener.ListAddedToSharedQueue( ( ListStatusData ) msg.obj );
                            break;
                        
                        case STATUS_LIST_ADDED_LOCAL_QUEUE:
                            m_statusListener.ListAddedToLocalQueue( ( ListStatusData ) msg.obj );
                            break;
                        
                        case STATUS_LIST_ADDED_CURRENT_LIST:
                            m_statusListener.ListAddedAsCurrentList( ( ListStatusData ) msg.obj );
                            break;
                        
                        case STATUS_CURRENT_LIST_DONE:
                            m_statusListener.CurrentListDone( ( ListStatusData ) msg.obj );
                            break;
                        
                        case STATUS_PACKAGE_PROCESSING_START:
                            m_statusListener.PackageProcessingStarted( ( ListStatusData ) msg.obj );
                            break;
                    }
                }
            };
            
        }
    }
    
    /**
     * Removes reference to class that implements RemoveOnListStatusChangedListener interface.
     */
    public void RemoveOnListStatusChangedListener() {
        m_statusListener = null;
        m_handler = null;
    }
    
    /**
     * Check is PackageManager started, ie. are workers created.
     *
     * @return True if Package Manager is started; false otherwise.
     */
    public boolean IsStarted() {
        return m_isStarted;
    }
    
    /**
     * Internal function. Called by Worker when it has added PackageList to its shared queue.
     *
     * @param statusData Info about Worker and PackageList.
     */
    public void _AddedToSharedQueue( ListStatusData statusData ) {
        if ( m_statusListener != null ) {
            
            Message message = m_handler.obtainMessage( STATUS_LIST_ADDED_SHARED_QUEUE, statusData );
            message.sendToTarget();
            
        }
    }
    
    /**
     * Internal function. Called by Worker when it has added PackageList to its local queue.
     *
     * @param statusData Info about Worker and PackageList.
     */
    public void _AddedToLocalQueue( ListStatusData statusData ) {
        if ( m_statusListener != null ) {
            
            Message message = m_handler.obtainMessage( STATUS_LIST_ADDED_LOCAL_QUEUE, statusData );
            message.sendToTarget();
            
        }
    }
    
    /**
     * Internal function. Called by Worker when it has added PackageList as current list.
     *
     * @param statusData Info about Worker and PackageList.
     */
    public void _AddedAsCurrentList( ListStatusData statusData ) {
        if ( m_statusListener != null ) {
            
            Message message = m_handler.obtainMessage( STATUS_LIST_ADDED_CURRENT_LIST, statusData );
            message.sendToTarget();
            
        }
    }
    
    /**
     * Internal function. Called by Worker when it has processed all Packages in current PackageList.
     *
     * @param statusData Info about Worker, PackageList and Package.
     */
    public void _CurrentListDone( ListStatusData statusData ) {
        if ( m_statusListener != null ) {
            
            Message message = m_handler.obtainMessage( STATUS_CURRENT_LIST_DONE, statusData );
            message.sendToTarget();
            
        }
    }
    
    /**
     * Internal function. Called by Worker when it starts processing new Package from current PackageList.
     *
     * @param statusData Info about Worker, PackageList and Package.
     */
    public void _PackageProcessingStarted( ListStatusData statusData ) {
        if ( m_statusListener != null ) {
            
            Message message = m_handler.obtainMessage( STATUS_PACKAGE_PROCESSING_START, statusData );
            message.sendToTarget();
            
        }
    }
    
    /**
     * Worker represents thread with infinite loop.
     */
    private class Worker extends Thread {
        
        // Thread for this worker.
        private Thread m_thread;
        // Lock for locking shared queue.
        private Lock m_lock;
        
        // Queue for storing package list. Accessed for writing by multiple threads and for reading only by this worker thread.
        private ArrayDeque<PackageList> m_sharedQueue;
        // Queue for storing package list. Accessed for writing and reading only by this worker thread.
        private ArrayDeque<PackageList> m_localQueue;
        
        // Monitor object used for thread synchronization.
        private final Object m_monitor = new Object();
        
        // Number of package lists currently in shared queue.
        private AtomicInteger m_listsInSharedQueue;
        
        // Package list whose packages this worker is currently processing.
        private PackageList m_currentList;
        
        // Worker thread name.
        private String m_threadName;
        // Integer ID of this worker.
        private int m_id;
        
        // Used to signal to stop worker thread.
        private boolean m_stop;
        // Used to signal if we should try to fetch new package lists from shared queue.
        private boolean m_fetchNewList;
        
        public Worker( int id, String name ) {
            m_id = id;
            m_threadName = name;
            
            m_lock = new ReentrantLock();
            
            m_sharedQueue = new ArrayDeque<>( 8 );
            m_localQueue = new ArrayDeque<>( 16 );
            
            m_listsInSharedQueue = new AtomicInteger( 0 );
            
            m_fetchNewList = true;
        }
        
        /**
         * This is Worker infinite loop. Code inside this function runs on worker thread.
         */
        @Override
        public void run() {
            while ( !m_stop ) {
                
                // Check to see if we should put thread to sleep.
                GoToWaiting();
                
                // Check if new packages were added to shared package list.
                FetchNewPackageList();
                
                // Get new package list from local queue.
                SetCurrentList();
                
                // DoOnWorkerThread packages we have in our local queue.
                ProcessPackages();
                
            }
        }
        
        /**
         * Tries to get new package list from shared queue.
         */
        private void FetchNewPackageList() {
            if ( m_fetchNewList ) {
                
                // We want ot get all the lists from shared queue.
                while ( true ) {
                    
                    if ( m_localQueue.isEmpty() ) {
                        
                        // If local queue is empty, lock and wait access to shared queue.
                        m_lock.lock();
                        
                    } else {
                        
                        // If local queue is not empty try to lock.
                        boolean isLocked = m_lock.tryLock();
                        
                        // If we fail to lock shared queue go back to processing next list from local queue. This means some
                        // thread is currently adding new list to shared queue and we will fetch it in next loop iteration.
                        if ( !isLocked ) {
                            return;
                        }
                        
                    }
                    
                    PackageManager.PackageList newList;
                    
                    // Unlock lock from finally statement ( queue could throw exception ).
                    try {
                        // Try to get new list from shared queue.
                        newList = m_sharedQueue.poll();
                    } finally {
                        // We have new list so release lock.
                        m_lock.unlock();
                    }
                    
                    if ( newList != null ) {
                        
                        // Decrement number of package lists that are now in shared queue.
                        m_listsInSharedQueue.decrementAndGet();
                        
                        ListStatusData statusData = new ListStatusData( m_id, m_threadName, newList.GetId(), newList.GetName() );
                        PackageManager.this._AddedToLocalQueue( statusData );
                        
                        // Add new list to local queue.
                        m_localQueue.offer( newList );
                        
                    } else {
                        
                        // Shared queue is empty so don't try to fetch any more lists from it.
                        m_fetchNewList = false;
                        
                        return;
                        
                    }
                }
            }
        }
        
        /**
         * Tries to put worker to sleep if there are no more package lists to process.
         */
        private void GoToWaiting() {
            // Try to put thread to sleep on when there are no more packages to process.
            if ( m_currentList == null && m_localQueue.isEmpty() ) {
                
                // Someone could interrupt thread while it is sleeping so we will use try statement.
                try {
                    // Only one thread can enter inside a synchronized block on the same monitor object.
                    synchronized ( m_monitor ) {
                        // Check conditions for sleeping to guard against lost notifications or spurious wakeup.
                        while ( m_listsInSharedQueue.get() == 0 && !m_stop ) {
                            m_monitor.wait();
                        }
                    }
                } catch ( InterruptedException ignored ) {
                }
            }
            
        }
        
        /**
         * Tries to get new package list from local queue and set it as current list for processing.
         */
        private void SetCurrentList() {
            if ( m_currentList == null ) {
                
                if ( !m_localQueue.isEmpty() ) {
                    
                    // If local queue is not empty get a new list from it.
                    m_currentList = m_localQueue.poll();
                    
                    ListStatusData statusData = new ListStatusData( m_id, m_threadName, m_currentList.GetId(), m_currentList.GetName() );
                    PackageManager.this._AddedAsCurrentList( statusData );
                    
                }
                
                // Local queue is empty so we need to get new package lists from shared queue.
                m_fetchNewList = true;
                
            }
        }
        
        /**
         * Processes packages from current package list.
         */
        private void ProcessPackages() {
            if ( m_currentList != null ) {
                
                // Try to get package from current list.
                Package p = m_currentList.GetCurrent();
                
                if ( p != null ) {
                    
                    ListStatusData statusData = new ListStatusData( m_id, m_threadName, m_currentList.GetId(), m_currentList.GetName(), p.GetId(), p.GetName() );
                    PackageManager.this._PackageProcessingStarted( statusData );
                    
                    // If we got package just process it.
                    p._Process();
                    
                } else {
                    
                    ListStatusData statusData = new ListStatusData( m_id, m_threadName, m_currentList.GetId(), m_currentList.GetName() );
                    PackageManager.this._CurrentListDone( statusData );
                    
                    // We have process all packages in current package list so null it.
                    m_currentList = null;
                    
                    // Signal that we want to get new lists from shared queue.
                    m_fetchNewList = true;
                    
                }
                
            }
        }
        
        /**
         * Adds new package list to worker shared queue.
         *
         * @param list Package list to Add to shared queue.
         */
        public void SubmitPackageList( PackageList list ) {
            // Acquire lock to Add package list to shared queue and release it when done.
            m_lock.lock();
            
            // Unlock lock from finally statement ( queue could throw exception ).
            try {
                m_sharedQueue.offer( list );
            } finally {
                ListStatusData statusData = new ListStatusData( m_id, m_threadName, list.GetId(), list.GetName() );
                PackageManager.this._AddedToSharedQueue( statusData );
                
                m_lock.unlock();
            }
            
            // Only one thread can enter inside a synchronized block on the same monitor object.
            synchronized ( m_monitor ) {
                // Increment number of package lists that are now in shared queue.
                m_listsInSharedQueue.incrementAndGet();
                
                // Notify thread to wake up if it sleeping.
                m_monitor.notify();
            }
        }
        
        /**
         * Makes this thread to begin execution. Multiple calls to this function have no effect.
         */
        public void Start() {
            if ( m_thread == null ) {
                
                // Create new Thread object and begin execution.
                m_thread = new Thread( this, m_threadName );
                m_thread.start();
                
            }
        }
        
        /**
         * Stops worker infinite loop and by doing so ends this thread.
         */
        public void Stop() {
            // Set worker loop condition to true and notify about it any thread that may be sleeping.
            synchronized ( m_monitor ) {
                m_stop = true;
                m_monitor.notify();
            }
        }
        
        /**
         * Returns name of this Worker thread.
         *
         * @return Thread name.
         */
        public String GetName() {
            return m_threadName;
        }
        
        /**
         * Returns ID of this Worker.
         *
         * @return Worker ID.
         */
        public int GetId() {
            return m_id;
        }
    }
    
    /**
     * Resizable list backed by array. PackageList does not have capacity restrictions; it grows as
     * necessary. Only one thread should Add elements to list but multiple threads can retrieve
     * elements. Null elements are prohibited.
     */
    public static class PackageList {
        
        /**
         * Implement this interface to get notified when all Packages in PackageList have been processed.
         */
        public interface OnProcessedListener {
            void OnProcessed();
        }
        
        // Interface reference.
        private OnProcessedListener m_listener;
        
        private static final int INITIAL_CAPACITY = 16;
        
        // Handler to communicate with android UI thread.
        private Handler m_handler;
        
        // Array of Package objects.
        private Package[] m_data;
        
        // Number of worker threads that have processed all packages in this list.
        private AtomicInteger m_workers;
        
        // Current package that is being processed.
        private AtomicInteger m_current;
        
        // Name of this list.
        private String m_name;
        private int m_id;
        
        // Total number of packages in this list.
        private int m_size;
        
        // Reference to PackageManager.
        public PackageManager m_owner;
        
        /**
         * Creates new package list with default capacity ( 16 ).
         *
         * @param workersCount Number of worker threads.
         */
        public PackageList( int workersCount ) {
            this( INITIAL_CAPACITY, workersCount );
        }
        
        /**
         * Creates new package list with given capacity.
         *
         * @param capacity     Capacity of this package list.
         * @param workersCount Number of worker threads.
         */
        public PackageList( int capacity, int workersCount ) {
            if ( capacity < 0 ) {
                throw new IllegalArgumentException( "List capacity cannot be negative number" );
            }
            
            m_workers = new AtomicInteger( workersCount );
            m_current = new AtomicInteger( 0 );
            
            m_data = new Package[capacity];
        }
        
        /**
         * Sets reference to OnProcessedListener interface.
         */
        public void SetOnProcessedListener( OnProcessedListener listener ) {
            m_listener = listener;
            
            if ( m_handler == null ) {
                
                m_handler = new Handler( Looper.getMainLooper() ) {
                    @Override
                    public void handleMessage( Message msg ) {
                        m_listener.OnProcessed();
                    }
                };
                
            }
        }
        
        /**
         * Nulls reference to OnProcessedListener interface.
         */
        public void RemoveOnProcessedListener() {
            m_listener = null;
            m_handler = null;
        }
        
        /**
         * Add a new element to the end of this list.
         *
         * @param p Element to append to this list.
         */
        public void Add( Package p ) {
            // Do not allow null variables in this list.
            if ( p == null ) {
                throw new IllegalArgumentException( "Supplied argument cannot be null" );
            }
            
            // If we are out of space in this list increase capacity.
            if ( m_size == m_data.length ) {
                IncreaseCapacity();
            }
            
            // Add data to list and increment list size.
            m_data[m_size++] = p;
        }
        
        /**
         * Send this PackageList to Workers for processing.
         */
        public void Submit() {
            for ( Worker worker : m_owner.m_workers ) {
                worker.SubmitPackageList( this );
            }
        }
        
        /**
         * Increases this list size to double the current capacity.
         */
        private void IncreaseCapacity() {
            // Get this list current capacity.
            int currentCapacity = m_data.length;
            
            // Create new array with double the current list capacity.
            Package[] newData = new Package[currentCapacity * 2];
            
            // Copy old data array to new data array.
            System.arraycopy( m_data, 0, newData, 0, currentCapacity );
            
            m_data = newData;
        }
        
        /**
         * Return element that is currently set for retrieving. It sets the element at the retrieved position to null.
         *
         * @return Element from this list or null if all elements have been already retrieved.
         */
        public Package GetCurrent() {
            // Get index of the current package and increment it for the next worker.
            int current = m_current.getAndIncrement();
            
            if ( current < m_size ) {
                
                // Return package and null the reference in list.
                Package t = m_data[current];
                m_data[current] = null;
                
                return t;
                
            } else {
                
                // All packages have been processed at this point.
                IsListProcessed();
                
                return null;
                
            }
        }
        
        /**
         * Check if all packages in this list were processed by all workers and call done callback.
         */
        private void IsListProcessed() {
            // Decrement number of workers that processed this list.
            int workers = m_workers.decrementAndGet();
            
            // If this is the last worker in this list, call done callback.
            if ( workers == 0 && m_listener != null ) {
                
                // Get a message object and send it to handler.
                Message message = m_handler.obtainMessage();
                message.sendToTarget();
                
            }
        }
        
        /**
         * Sets this PackageList ID.
         *
         * @param id New ID.
         */
        public void SetId( int id ) {
            m_id = id;
        }
        
        /**
         * Returns this PackageList ID.
         */
        public int GetId() {
            return m_id;
        }
        
        /**
         * Sets this PackageList name.
         *
         * @param name New name.
         */
        public void SetName( String name ) {
            m_name = name;
        }
        
        /**
         * Returns this PackageList name.
         */
        public String GetName() {
            return m_name;
        }
        
    }
    
    /**
     * Holds data about worker, package list and package. Used only by OnStatusChangedListener interface.
     */
    public class ListStatusData {
        public int m_workerId;
        public String m_workerName;
        
        public int m_listId;
        public String m_listName;
        
        public int m_packageId;
        public String m_packageName;
        
        public ListStatusData( int workerId, String workerName, int listId, String listName ) {
            this( workerId, workerName, listId, listName, 0, null );
        }
        
        public ListStatusData( int workerId, String workerName, int listId, String listName, int packageId, String packageName ) {
            m_workerId = workerId;
            m_workerName = workerName;
            
            m_listId = listId;
            m_listName = listName;
            
            m_packageId = packageId;
            m_packageName = packageName;
        }
    }
}
