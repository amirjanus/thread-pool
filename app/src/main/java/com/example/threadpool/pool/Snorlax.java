package com.example.threadpool.pool;

/**
 * This is the class that will executed on Worker thread.
 */
public class Snorlax extends Package {
    
    public Snorlax( int id, String name ) {
        super( id, name );
    }
    
    /**
     * Thread that runs this function will sleep for 1 second.
     *
     * @return Doesn't have return result so return null.
     */
    @Override
    public Object Process() {
        try {
            Thread.sleep( 1000 );
        } catch ( InterruptedException ignored ) {
        }
        
        return null;
    }
    
}
